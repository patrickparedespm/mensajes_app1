
package mensajes_app1;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Patrick Paredes
 */
public class MensajeService {
      private static Scanner sc=new Scanner(System.in); 
    public static void crearMensaje(){
      
        System.out.println("Escribir mensaje");
        String mensaje=sc.nextLine(); 
        
        System.out.println("Autor: ");
        String nombre=sc.nextLine(); 
        
        Mensajes registro= new Mensajes();
        registro.setMensaje(mensaje); 
        registro.setAutor_mensaje(nombre); 
        
        MensajesDAO.CrearMensajeDB(registro);
    
    
    }
    public static void listarMensaje(){
        try {
            ArrayList<Mensajes> listaMensajes=MensajesDAO.ListarMensajesDB();
            for(Mensajes mensajeActual:listaMensajes){
                System.out.println("***************************************");
                System.out.println("fecha:"+mensajeActual.fecha_mensaje);
                System.out.println("Autor:"+mensajeActual.autor_mensaje);
                System.out.println("el mensaje es:"+mensajeActual.mensaje);
                System.out.println("***************************************");
            }
                 
        } catch (NullPointerException e) {
            System.out.println("no existen registros de msm");
        }
        
        
    }
    public static void editarMensaje(){
    
        
        System.out.println("Ingrese el id");
        String id=sc.nextLine();         
        System.out.println("ingresar el contenido: ");
        String contenido=sc.nextLine(); 
        
        MensajesDAO.editarMensaje(id, contenido);
        
        
    }
    public static void borrarMensaje(){
    
     System.out.println("Ingrese el id");
        String id=sc.nextLine();
        
        MensajesDAO.eliminarMensaje(id);
    }
    
    
    
    
    
    
    
    
    
}
