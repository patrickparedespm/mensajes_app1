
package mensajes_app1;
import java.sql.Connection;
import java.sql.PreparedStatement;

import java.sql.ResultSet;
import java.sql.Statement;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 *
 * @author Patrick Paredes
 */
public class MensajesDAO { 
     private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
    public static void CrearMensajeDB(Mensajes mensaje){
    Conexion db_connect= new Conexion(); 
        try (Connection conexion=db_connect.get_connection()){
            PreparedStatement ps =null;                     
                Timestamp timestamp=new  Timestamp(System.currentTimeMillis()) ;
                String query="INSERT INTO mensajes (mensaje,autor_mensaje,fecha_mensaje)"
                        + " VALUES(?,?,?)";                  
                ps=conexion.prepareStatement(query);
                ps.setString(1,mensaje.mensaje); 
                ps.setString(2,mensaje.autor_mensaje);
                ps.setString(3,sdf.format(timestamp));
                ps.executeUpdate();
                System.out.println("--Mensaje Creado Exitoso--");         
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    
        public static ArrayList<Mensajes> ListarMensajesDB()throws NullPointerException{
            Conexion db_connect= new Conexion(); 
             ArrayList<Mensajes> listaMensajes= new ArrayList<>();
            try (Connection conexion=db_connect.get_connection()){
                Statement statement = null;                                           
                String query="SELECT * FROM mensajes";                  
                statement=conexion.createStatement();
                ResultSet rs=statement.executeQuery(query);                
              while (rs.next()){
                    Mensajes mensajeActual=new Mensajes();
                    mensajeActual.setFecha_mensaje(rs.getString("fecha_mensaje"));                    
                    mensajeActual.setAutor_mensaje(rs.getString("autor_mensaje"));
                    mensajeActual.setId_mensaje(rs.getInt("id_mensaje"));
                    mensajeActual.setMensaje(rs.getString("mensaje"));
                    listaMensajes.add(mensajeActual);
                }
                System.out.println("--Mensaje Creado Exitoso--");
                return listaMensajes;                
          
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }           
        } 
        
        
            
     
        
        public static void eliminarMensaje(String idMensaje){
           Conexion db_connect= new Conexion(); 
        try (Connection conexion=db_connect.get_connection()){
            PreparedStatement ps =null;                     
                Timestamp timestamp=new  Timestamp(System.currentTimeMillis()) ;
                String query="DELETE FROM mensajes WHERE id_mensaje=?;";                  
                ps=conexion.prepareStatement(query);
                ps.setString(1,idMensaje);                          
                ps.executeUpdate();
                System.out.println("--Mensaje eliminado Exitoso--");         
        } catch (Exception e) {
            System.out.println(e);
        }
        
        }
    
        public static void editarMensaje(String idMensaje,String contenidoMensaje){
           Conexion db_connect= new Conexion(); 
        try (Connection conexion=db_connect.get_connection()){
            PreparedStatement ps =null;                     
                Timestamp timestamp=new  Timestamp(System.currentTimeMillis()) ;
                String query="UPDATE mensajes SET mensaje=? WHERE id_mensaje=?";                  
                ps=conexion.prepareStatement(query);
                ps.setString(1,contenidoMensaje); 
                ps.setString(2,idMensaje);                
                ps.executeUpdate();
                System.out.println("--Mensaje actualizado Exitoso--");         
        } catch (Exception e) {
            System.out.println(e);
        }
        
        }
    
    
}
